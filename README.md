# SQL Client Assignment

Quick and easy SQL client for accessing data in a database. Has some nice methods that should handle most necessary data collection tasks.
It is using repository standards.

## Installation

Install Microsoft.Data.SqlClient

```bash
dotnet add [PROJECT NAME] package Microsoft.Data.SqlClient --version 3.0.0
```

## Usage

See summary tags in code :)

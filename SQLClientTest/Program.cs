﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;

namespace SQLClientTest
{
    class Program
    {

        /// <summary>
        /// CreateNewCustomer creates a new customer with values from params.
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="country"></param>
        /// <param name="postalCode"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <returns>A Customer object</returns>
        public static Customer CreateNewCustomer(string firstName, string lastName, string country, string postalCode, string phone, string email)
        {
            Customer customer = new();
            customer.FirstName = firstName;
            customer.LastName = lastName;
            customer.Country = country;
            customer.PostalCode = postalCode;
            customer.Phone = phone;
            customer.Email = email;

            return customer;
        }

        /// <summary>
        /// Using and logging all of the methods in CustomerRepository.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            CustomerRepository repo = new();
            Customer user = repo.GetCustomerByID("1");
            List<Customer> user2 = repo.GetCustomerByName("Leonie");
            List<Customer> pageOfCustomers = repo.GetPageOfCustomers(50, 50);
            List<CustomerSpender> customerSpenders = repo.HighestSpenders();
            List<CustomerGenre> customerGenres = repo.MostPopularGenre(12);
            List<CustomerCountry> customerCountries = repo.CustomersPerCountry();

            Customer newCustomer = CreateNewCustomer("Jacob", "Robertson", "Norway", "5160", "12345678", "bob@email.com");
            Customer updatedCustomer = CreateNewCustomer("Benjamin", "Robertson", "Norway", "5160", "12345678", "bob@email.com");

            // Run these to add or update to database
            bool added = repo.AddNewCustomer(newCustomer);
            bool updated = repo.UpdateCustomer(updatedCustomer, "60");
            Console.WriteLine(added);
            Console.WriteLine(updated);

            // Logging to console.
            Console.WriteLine($"{user.FirstName} {user.LastName}");

            foreach (Customer customer in user2)
            {
                Console.WriteLine($"{customer.FirstName} {customer.LastName}");
            }
            
            foreach (Customer customer in pageOfCustomers)
            {
                Console.WriteLine($"{customer.FirstName}");
            }

            foreach (CustomerCountry customerCountry in customerCountries)
            {
                Console.WriteLine($"{customerCountry.Country}: {customerCountry.Customers}");
            }

            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                Console.WriteLine($"{customerSpender.ID}: {customerSpender.Total}");
            }

            foreach (CustomerGenre customerGenre in customerGenres)
            {
                Console.WriteLine($"{customerGenre.CustomerName}({customerGenre.CustomerId}): {customerGenre.Genre}, Count: {customerGenre.Count}");
            }
        }
    }
}

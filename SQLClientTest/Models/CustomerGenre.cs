﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientTest
{
    public class CustomerGenre
    {
        public string Genre { get; set; }
        public int Count { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

    }
}

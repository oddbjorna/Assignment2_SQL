﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientTest
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Customers { get; set; }
    }
}

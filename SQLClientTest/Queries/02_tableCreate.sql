CREATE TABLE Superhero(
	HeroID int IDENTITY(1,1) PRIMARY KEY,
	HeroName nvarchar(50),
	HeroAlias nvarchar(50),
	HeroOrigin nvarchar(50)
);

CREATE TABLE Assistant(
	AssistantID int IDENTITY(1,1) PRIMARY KEY,
	AssitantName nvarchar(50)
);

CREATE TABLE SuperPower(
	PowerID int IDENTITY(1,1) PRIMARY KEY,
	PowerName nvarchar(50),
	PowerDescription nvarchar(100)
);
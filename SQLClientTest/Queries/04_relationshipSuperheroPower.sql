CREATE TABLE SuperheroPowerLink (
	HeroID int,
	PowerID int,
	PRIMARY KEY (HeroID, PowerID),
	CONSTRAINT fk_SuperheroID FOREIGN KEY (HeroID) REFERENCES Superhero(HeroID),
	CONSTRAINT fk_PowerID FOREIGN KEY (PowerID) REFERENCES SuperPower(PowerID)
);

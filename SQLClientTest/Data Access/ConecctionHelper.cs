﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientTest
{
    public class ConnectionHelper
    {

        // Change this to your computer name :))
        private static string ConnectedComputer { get; set; } = "DESKTOP-S3TOROT";

        /// <summary>
        /// GetConnectionString builds our connection string to connect to the database.
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = $"{ConnectedComputer}\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            return builder.ConnectionString;
        }
    }
}

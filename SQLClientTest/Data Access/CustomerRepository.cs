﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientTest
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Helper method for creating a customer to be used in later methods.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns>A Customer object</returns>
        private static Customer CreateCustomer(SqlDataReader reader)
        {
            Customer customer = new();
            customer.CustomerId = reader.GetInt32(0);
            customer.FirstName = reader.GetString(1);
            customer.LastName = reader.GetString(2);
            customer.Country = reader.GetString(3);
            customer.PostalCode = reader.GetString(4);
            customer.Phone = reader.GetString(5);
            customer.Email = reader.GetString(6);

            return customer;
        }

        /// <summary>
        /// Helper method to add correct values to a command object to be used in later methods.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="customer"></param>
        /// <returns>A boolean to check if the customer was added correctly</returns>
        private static bool AddParameters(SqlCommand command, Customer customer)
        {
            command.Parameters.AddWithValue("@FirstName", customer.FirstName);
            command.Parameters.AddWithValue("@LastName", customer.LastName);
            command.Parameters.AddWithValue("@Country", customer.Country);
            command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
            command.Parameters.AddWithValue("@Phone", customer.Phone);
            command.Parameters.AddWithValue("@Email", customer.Email);
            bool success = command.ExecuteNonQuery() > 0 ? true : false;

            return success;
        }

        /// <summary>
        /// GetAllCustomers fetches all the customers from the database.
        /// </summary>
        /// <returns>A List of Customer objects</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    customerList.Add(CreateCustomer(reader));
                                }
                                catch (SqlNullValueException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// GetCustomerByID selects a specific Customer based on an ID input.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>A Customer object</returns>
        public Customer GetCustomerByID(string ID)
        {
            Customer customer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerID", ID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    customer = CreateCustomer(reader);
                                }
                                catch (SqlNullValueException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        /// <summary>
        /// GetCustomerByName selects all the Customers that has a name similar to the inputed "name".
        /// </summary>
        /// <param name="name"></param>
        /// <returns>A list of customer objects</returns>
        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> customers = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @FirstName";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", name);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    customers.Add(CreateCustomer(reader));
                                }
                                catch (SqlNullValueException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// GetPageOfCustomers selects a collection of customers based on a limit and an offset input.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>A list of Customer objects</returns>
        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            {
                List<Customer> customerList = new();
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

                try
                {
                    using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                    {
                        connection.Open();
                        using (SqlCommand command = new(sql, connection))
                        {
                            command.Parameters.AddWithValue("@limit", limit);
                            command.Parameters.AddWithValue("@offset", offset);
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    try
                                    {
                                        customerList.Add(CreateCustomer(reader));
                                    }
                                    catch (SqlNullValueException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine(customerList);
                return customerList;
            }
        }

        /// <summary>
        /// AddNewCustomer inserts a new customer into the SQL database. 
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>A bool to signify if the customer wass added correctly</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new(sql, connection))
                    {
                        success = AddParameters(command, customer);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }

        /// <summary>
        /// UpdateCustomer is updating a given customer in the database. 
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="id"></param>
        /// <returns>A boolean value that says if the customer was updated correctly</returns>
        public bool UpdateCustomer(Customer customer, string id)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @ID";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@ID", id);
                        success = AddParameters(command, customer);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }

        /// <summary>
        /// CustomerPerCountry fetches all the countries and the amount of customers from those countries.
        /// </summary>
        /// <returns>A list of CustomerCountry objects</returns>
        public List<CustomerCountry> CustomersPerCountry()
        {
            List<CustomerCountry> customerCountries = new();
            
            string sql = "SELECT country, COUNT(*) AS number FROM Customer GROUP BY country ORDER BY number DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.Customers = reader.GetInt32(1);
                                customerCountries.Add(customerCountry);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerCountries;
        }

        /// <summary>
        /// HighestSpenders selects the sum of total invoices for each customer.
        /// </summary>
        /// <returns>A list of CustomerSpender objects.</returns>
        public List<CustomerSpender> HighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new();
            string sql = "SELECT CustomerId, SUM(Total) AS sum FROM Invoice GROUP BY CustomerId ORDER BY sum DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new();
                                customerSpender.ID = reader.GetInt32(0);
                                customerSpender.Total = reader.GetDecimal(1);
                                customerSpenders.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpenders;
        }

        /// <summary>
        /// MostPopularGenre selects a customers most bought tracks that corresponds to a genre, based on invoices.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>A list of CustomerGenre objects</returns>
        public List<CustomerGenre> MostPopularGenre(int customerId)
        {
            List<CustomerGenre> customerGenres = new();
            
            string sql =
                "SELECT TOP 1 WITH TIES Customer.CustomerId, Customer.FirstName, Genre.Name, COUNT(Genre.GenreId) AS number " +
                "FROM ((((Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId) " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId) " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId) " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId) " +
                "WHERE Customer.CustomerId = @CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Genre.Name " +
                "ORDER BY number DESC ";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customerId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new();
                                customerGenre.CustomerId = reader.GetInt32(0);
                                customerGenre.CustomerName = reader.GetString(1);
                                customerGenre.Genre = reader.GetString(2);
                                customerGenre.Count = reader.GetInt32(3);

                                customerGenres.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenres;
        }
    }
}
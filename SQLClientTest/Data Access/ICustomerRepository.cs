﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClientTest
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetCustomerByID(string id);
        public List<Customer> GetCustomerByName(string name);
        public List<Customer> GetPageOfCustomers(int limit, int offset);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer, string id);
        public List<CustomerCountry> CustomersPerCountry();
        public List<CustomerSpender> HighestSpenders();
        public List<CustomerGenre> MostPopularGenre(int customerId);
    }
}
